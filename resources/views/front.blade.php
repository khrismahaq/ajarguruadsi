@extends('layouts.app')

@section('content')

<div class="container">
    <!-- jumbotron -->
    <section>
        <div class="pb-3 mb-4 bg-white rounded-3">
            <div class="container px-4">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6">
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                            <h1 class="display-5 fw-bold">AjarGuru</h1>
                            <p class="col-md-8 fs-4">
                                Platform penyedia bahan ajar dan tata cara mengajar untuk guru Pendidikan Anak Usia Dini dan Taman Kanak-Kanak.
                            </p>
                            <div class="pt-5">
                            <a href="{{route('series.index')}}" class="btn btn-primary btn-lg">Akses Bahan Ajar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="guru">
                            <img src="/img/guru.png" class="card-img-top" alt="Guru">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="container">
    <!-- card -->
    <section>
        <div class="row">
            @foreach($featuredSeries as $series)
            <div class="col-4">
                <div class="card" style="width: 20rem;">
                    <img src="img/alpabet.jpg" class="card-img-top" alt="alpabet">
                    <div class="card-body">
                        <h5 class="card-title">{{$series->title}}</h5>
                        <p class="card-text">{{\Str::words($series->description, 6)}}</p>
                        <a href="#" class="btn btn-primary">Kunjungi Bahan Ajar</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
</div>

@endsection