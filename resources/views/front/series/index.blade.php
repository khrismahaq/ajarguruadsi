@extends('layouts.app')

@section('content')

<div class="container">
    <!-- card -->
    <section>
        <div class="row">
            @foreach($series as $s)
            <div class="col-4 mt-5">
                <div class="card text-center h-100">
                    <img src="img/alpabet.jpg" class="card-img-top" alt="alpabet">
                    <div class="card-body">
                        <h5 class="card-title">{{$s->title}}</h5>
                        <p class="card-text">{{$s->description}}</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('series.show', $s->id)}}" class="btn btn-primary">Putar</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
</div>

@endsection