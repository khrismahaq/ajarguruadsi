@extends('layouts.app')

@section('content')

<div class="container">
    <!-- jumbotron -->
    <section>
        <div class="pb-3 mb-4 bg-white rounded-3">
            <div class="container px-4">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6">
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                            <h1 class="display-5 fw-bold">{{$series->title}}</h1>
                            <p class="col-md-8 fs-4">
                                {{$series->description}}
                            </p>
                            <div class="pt-5">
                                <a href="{{route('series.index')}}" class="btn btn-primary btn-lg">Mulai Series</a>
                                <a href="{{route('series.index')}}" class="btn btn-success btn-lg">Berlangganan</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="guru">
                            <img src="/img/belajar.png" class="card-img-top" alt="belajar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mb-5">
        <h3 class="mb-3 text-center">Episodes</h3>
        <episodes :videos="{{$series->videos}}"></episodes>
    </section>

    <!-- card episode -->
    <section>
        <div class="card mb-3">
            <div class="row g-0">
                <div class="col-md-3">
                    <img src="/img/angka.jpg" class="img-fluid rounded-start" alt="img">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-3">
            <div class="row g-0">
                <div class="col-md-3">
                    <img src="/img/baca.jpg" class="img-fluid rounded-start" alt="img">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>